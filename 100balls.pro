#This is the basic qmake template for the Ubuntu-SDK
#Now if you want to get started right click the project
#and add the subproject you like (app,scope,helper,backend)

TEMPLATE = subdirs

SUBDIRS += 100balls \
    manifest

#load Ubuntu specific features
load(ubuntu-click)

# specify translation domain, this must be equal with the
# app name in the manifest file
UBUNTU_TRANSLATION_DOMAIN="100balls.jonnius"

# specify the source files that should be included into
# the translation file, from those files a translation
# template is created in po/template.pot, to create a
# translation copy the template to e.g. de.po and edit the sources
UBUNTU_TRANSLATION_SOURCES+= \
    $$files(100balls/*.qml,true) \
    $$files(100balls/*.js,true)

# specifies all translations files and makes sure they are
# compiled and installed into the right place in the click package
UBUNTU_PO_FILES+=$$files(po/*.po)
