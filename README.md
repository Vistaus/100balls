# 100 Balls

Open source clone of 100 Balls for Ubuntu for Phones

Based on Bacon2D, a framework to ease 2D game development, providing
ready-to-use QML components useful for creating compeling games.

## Installation

### On Ubuntu Touch
To build the click package, install [clickable](https://github.com/bhdouglass/clickable). Simply run `clickable` from the app directory. See [clickable documentation](http://clickable.bhdouglass.com/) for details.

### On desktop

You need Qt 5.x and [Bacon2D][bacon] installed on your system.
Then you can clone this repository, and launch the game executing

`qmlscene 100balls/100balls.qml`

in the root of the project

## Contribution

If you want to help with developement, or you have a suggestion, open an issue
on this repository.

## Thanks

This game wouldn't be possible without help of these developers:

- Riccardo Padovani
- Alan Pope
- Ken VanDine
- Nekhelesh Ramananthan
- Stefano Verzegnassi

and with the support of all guys of the Ubuntu Community.

Thanks to all!

[bacon]: https://github.com/Bacon2D/Bacon2D
